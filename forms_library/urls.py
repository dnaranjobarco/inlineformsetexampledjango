from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^model_formset/', 'library.views.model_formset', name='multiple_model_formset'),
    url(r'^inline_formset/', 'library.views.inline_formset', name='multiple_inline_formset'),
)
