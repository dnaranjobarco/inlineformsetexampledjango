from django.contrib import admin
from library.models import Tarjeta, Persona

admin.site.register(Tarjeta)
admin.site.register(Persona)
