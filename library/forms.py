from django import forms
from library.models import Tarjeta, Persona

from django.forms.models import inlineformset_factory, modelformset_factory


class CharFieldInvisibleBorder(forms.CharField):
    widget = forms.TextInput(attrs={'class': 'invisible_border'})


class TarjetaForm (forms.ModelForm):

    class Meta:
        model = Tarjeta
        fields = ['name']


class PersonaForm (forms.ModelForm):

    class Meta:
        model = Persona
        fields =['nombre']


InlinePersonaFormSet = inlineformset_factory(Tarjeta, Persona, form=PersonaForm, can_delete=False,extra=2)

PersonaFormSet = modelformset_factory(Persona, extra=1)
