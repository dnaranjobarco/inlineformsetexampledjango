from django.db import models


class Tarjeta (models.Model):
    name = models.CharField(max_length=130)

    def __unicode__(self):
        return self.name

class Persona (models.Model):
    tarjeta = models.ForeignKey(Tarjeta)
    nombre = models.CharField(max_length=130)

    def __unicode__(self):
        return self.nombre







