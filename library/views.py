from django.http import HttpResponse
from django.shortcuts import render
from library.models import TarjetaForm, PersonaFormSet, Persona, InlinePersonaFormSet


def model_formset(request):
    formset = PersonaFormSet(queryset=Persona.objects.none())
    if request.method == 'POST':
        formset = PersonaFormSet(request.POST)
        if formset.is_valid():
            formset.save()
            return HttpResponse('creado')
        else:
            print formset.errors
    context = {'formset': formset}
    return render(request, 'library/model_formset.html', context)


def inline_formset(request):
    tarjeta_form = TarjetaForm()
    inline_form = InlinePersonaFormSet(queryset=Persona.objects.none())
    if request.method == 'POST':
        tarjeta_form = TarjetaForm(request.POST)
        if tarjeta_form.is_valid():
            tarjeta = tarjeta_form.save()
            inline_form = InlinePersonaFormSet(request.POST, instance=tarjeta)
            if inline_form.is_valid():
                inline_form.save()
                return HttpResponse('creado')
            else:
                print inline_form.errors
        else:
                print tarjeta_form.errors
    context = {'tarjeta_form': tarjeta_form, 'inline_form': inline_form}
    return render(request, 'library/inline_formset.html', context)