function ocultar_capa(){
	   $("#ocultar").click(function(evento){
			 $("#prueba").css("display", "none");
			 $('#myTable > tbody:last').append('<tr><td>column 1 value</td><td>column 2 value</td></tr>');

	   });
	}
	
	
var numCampos = 1;
	
	function new_child(nombre){
		$("#tabla_childs").css('display', 'true');
		$('#tabla_childs > tbody:last').append('<tr><td><input size=25 type="text" id="nombre"/>'+'</td><td><input size=5 type="text" id="trabajo"/></td></tr>');
		
	}
	
		function new_adult(nombre){
		$("#tabla_adults").css('display', 'true');
		$('#tabla_adults > tbody:last').append('<tr><td><input size=25 type="text" id="nombre"/>'+'</td><td><input size=5 type="text" id="guarderia"/></td></tr>');
		
	}
	
	function new_person(){
		$("#input_edad").focusout(function(evento){
		var edad = $("#input_edad").val();
			if (edad >0 && edad < 20)
				new_child();
			if (edad >=20 && edad < 150)
				new_adult();
			//$('#tabla_personas > tbody:last').append('<tr><td><input size=25 type="text" id="nombre' + numCampos+ '"/></td><td><input size=5 type="text" id="edad"/></td></tr>');
			//$("#nombre"+numCampos).focus();
			//numCampos++;
	   });
	}
	
	function insert_line(){
		$("#boton_new").click(function(evento){
			 //$("#input_especial").css("display", "none");
			 $('#books_table > tbody:last').append('<tr><td> Title: <input type="text" name="book_'+numCampos+'" id="book_'+numCampos+'"></td></tr>');
			$("#book_"+numCampos).focus();
			numCampos++;
	   });
	}
	
	function some_stuff(){
		$("#tabla_childs").css('display', 'none');
		$("#tabla_adults").css('display', 'none');
		new_person();
		insert_line();	   
		
	}




	function updateElementIndex(el, prefix, ndx) {
		var id_regex = new RegExp('(' + prefix + '-\\d+)');
		var replacement = prefix + '-' + ndx;
		if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
		if (el.id) el.id = el.id.replace(id_regex, replacement);
		if (el.name) el.name = el.name.replace(id_regex, replacement);
	}

    function addForm(btn, prefix) {
        var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
        var row = $('.dynamic-form:first').clone(true).get(0);
        $(row).removeAttr('id').insertAfter($('.dynamic-form:last')).children('.hidden').removeClass('hidden');
        $(row).children().not(':last').children().each(function() {
    	    updateElementIndex(this, prefix, formCount);
    	    $(this).val('');
        });
        $(row).find('.delete-row').click(function() {
    	    deleteForm(this, prefix);
        });
        $('#id_' + prefix + '-TOTAL_FORMS').val(formCount + 1);
        return false;
    }

    function deleteForm(btn, prefix) {
        $(btn).parents('.dynamic-form').remove();
        var forms = $('.dynamic-form');
        $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
        for (var i=0, formCount=forms.length; i<formCount; i++) {
    	    $(forms.get(i)).children().not(':last').children().each(function() {
    	        updateElementIndex(this, prefix, i);
    	    });
        }
        return false;
    }